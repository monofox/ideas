import Icon from './img/ideas.svg';

export default {
	id: "ideas",
	meta: {
		title: 	{
			"de": "Ideen-Sammlung",
			"en": "Brainstorming",
		},
		text:	{
			"de": "Gemeinsames Sammeln von Ideen - digitales Brainstorming",
			"en": "Collecting ideas together - digital brainstorming"
		},
		to: "ideas-index",
		adminto: "ideas-admin",
		icon: Icon,
		role: "ideas",
		index: true
	},
	routes: [
		{	path: '/ideas-index', name:'ideas-index', component: () => import('./views/Index.vue') },
		{	path: '/ideas', name:'ideas-part', component: () => import('./views/Part.vue') },
		{	path: '/ideas/:ticket', name:'ideas-part-ticket', component: () => import('./views/Part.vue') },
		{	path: '/ideas-edit', name:'ideas-edit', component: () => import('./views/Edit.vue') },
		{	path: '/ideas-slideshow', name:'ideas-slideshow', component: () => import('./views/Slideshow.vue') },
		{	path: '/ideas-admin', name:'ideas-admin', component: () => import('./views/Admin.vue') },
	],
	uploadMaxSize: 5*1024*1024,
	imageMaxSize: 0.5*1024*1024
}
