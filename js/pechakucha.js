var numSlides=0;
var slide=0;
var slideTime=0;
var timer;
const TIME_SLIDE=20;
const TIME_ANIMATE=0.5;

function tick() {
	if (slide==0) return;
	slideTime++;
	if (slideTime>=TIME_SLIDE) {
		nextSlide();
	}
	else {
		var progress=(slideTime+(slide-1)*TIME_SLIDE)/(numSlides*TIME_SLIDE);
		$('#progress').width(Math.round(progress*1000)/10+'%')
	}
}

/*
 * Show next slide
 */
function nextSlide() {
	//fade out old slide
	$('#slide'+slide).fadeOut(TIME_ANIMATE*1000);
	//Stop active media
	var media=$('#media'+slide)[0];
	if (media) {
		media.pause();
		media.currentTime=0
	}
	//increment counter
	slide++;
	slideTime=0;
	if (slide<=numSlides) {
		//Start next media (if neccessary)
		var media=$('#media'+slide)[0];
		if (media) media.play();
		//fade in current slide
		$('#slide'+slide).fadeIn(TIME_ANIMATE*1000);
	}
	//finished, show start page
	else {
		$('#slide0').show();
		slide=0;
	}
	var progress=(slide-1)/numSlides;
	$('#progress').width(Math.round(progress*1000)/10+'%');
}

$(document).ready(function () {
	numSlides=$('#numSlides').val();
	$('div.slide').click(nextSlide);
	$('div.slide').hide();
	$('#slide0').fadeIn(TIME_ANIMATE*1000);
	setInterval(tick, 1000);
})
